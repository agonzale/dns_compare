#!/usr/bin/env python3
#
'''
    Recevies two DNS names, name 1 and name 2. It makes sure that all name 1's ips
match with an ip of name 2. This is done both for ip4 and ip6. This is used to make
sure that NAME1 is an alias of NAME2, when like with its.cern.ch, we cannot use CNAMEs.
'''

import socket
import sys
from os import environ

# sendmail
import smtplib

def send_mail(from_addr, to_addr, subject, msg):
    '''
        Sends an email. user from_addr as origin, to_addr as destination, msg and message
    '''

    msg = "From: %s\r\nTo: %s\r\nSubject: %s\r\n\r\n%s" % (from_addr, to_addr, subject, msg)

    server = smtplib.SMTP(CERNMX, MXPORT)
    server.ehlo()
    server.sendmail(from_addr, to_addr, msg)
    server.quit()

def check_ip(name1, name2, family=socket.AF_INET):
    '''
        Checks that all IPs in name1 points to any of the ips of name2, and that both exist. By
        default it checks it for IPv4 (AF_INET), but it can configured to do it for IPv6 (AF_INET6)
    '''

#for family in (socket.AF_INET, socket.AF_INET):
    rcode = 0
    error_msg = ""

    pretty_family = "Ipv4" if family == 2 else "IPv6"

    try:
        results1 = socket.getaddrinfo(name1, 80, family, socket.SOCK_STREAM)
    except socket.gaierror as gai:
        error_msg += '"%s"(%s) %s\n' % (name1, pretty_family, gai)
        rcode = 1

    try:
        results2 = socket.getaddrinfo(name2, 80, family, socket.SOCK_STREAM)
    except socket.gaierror as gai:
        error_msg += '"%s"(%s) %s\n' % (name2, pretty_family, gai)
        rcode = 2

    if rcode != 0:
        return(rcode, error_msg)

    ips2 = ()

    for result in results2:
        ips2 += (result[4][0],)


    for result in results1:
        ip1 = result[4][0]
        if ip1 not in ips2:
            error_msg += '%s not in %s\n' %(ip1, ips2)
            rcode = 1

    return (rcode, error_msg)

######
RCODE = 0
#
# Mail
CERNMX = 'cernmx.cern.ch'
MXPORT = 25

try:
    NAME1 = sys.argv[1]
    NAME2 = sys.argv[2]
except IndexError:
    print("Use: %s <NAME1> <NAME2>" % sys.argv[0])
    sys.exit(1)

try:
    FROM = environ['FROM']
    TO = environ['TO']
except KeyError as kerr:
    print("Missing enviroment variable %s" % kerr)
    sys.exit(5)

ERROR_MSG = ""

(RCODE4, ERROR_MSG4) = check_ip(NAME1, NAME2, family=socket.AF_INET)

# Disabled because ipv6 does not work
(RCODE6, ERROR_MSG6) = check_ip(NAME1, NAME2, family=socket.AF_INET6)
RCODE = RCODE4 + RCODE6
ERROR_MSG = ERROR_MSG4 + ERROR_MSG6

if RCODE != 0:
    print(ERROR_MSG, file=sys.stderr)
    send_mail(FROM, TO, "Error(%d): dns_compare.py %s %s" % (RCODE, NAME1, NAME2), ERROR_MSG)

sys.exit(0)
